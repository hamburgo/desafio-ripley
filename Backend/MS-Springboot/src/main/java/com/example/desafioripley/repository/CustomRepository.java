package com.example.desafioripley.repository;

import com.example.desafioripley.io.swagger.model.TransfersResponse;

public interface CustomRepository {
    TransfersResponse getAllTransfers();
}
