package com.example.desafioripley.io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Notification
 */
@Validated


public class Notification   {
  @JsonProperty("category")
  private String category = null;

  @JsonProperty("code")
  private String code = null;

  @JsonProperty("message")
  private String message = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("field_name")
  private String fieldName = null;

  @JsonProperty("action")
  private String action = null;

  @JsonProperty("timestamp")
  private LocalDateTime timestamp = null;

  @JsonProperty("severity")
  private String severity = null;

  public Notification category(String category) {
    this.category = category;
    return this;
  }

  /**
   * Category of this notification as a Http status
   * @return category
   **/
  @ApiModelProperty(example = "BAD_REQUEST", value = "Category of this notification as a Http status")
  
    public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public Notification code(String code) {
    this.code = code;
    return this;
  }

  /**
   * Notification code. First character indicates severity (E,W,I).
   * @return code
   **/
  @ApiModelProperty(example = "BAD REQUEST", required = true, value = "Notification code. First character indicates severity (E,W,I).")
      @NotNull

    public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Notification message(String message) {
    this.message = message;
    return this;
  }

  /**
   * Description of the error or information notification. Where the response is 2xx, it's a notification with informational details about this response.
   * @return message
   **/
  @ApiModelProperty(example = "Something is invalid", value = "Description of the error or information notification. Where the response is 2xx, it's a notification with informational details about this response.")
  
    public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Notification description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Message which explains the notification.
   * @return description
   **/
  @ApiModelProperty(value = "Message which explains the notification.")
  
    public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Notification fieldName(String fieldName) {
    this.fieldName = fieldName;
    return this;
  }

  /**
   * Field name in request this notification is related to.
   * @return fieldName
   **/
  @ApiModelProperty(value = "Field name in request this notification is related to.")
  
    public String getFieldName() {
    return fieldName;
  }

  public void setFieldName(String fieldName) {
    this.fieldName = fieldName;
  }

  public Notification action(String action) {
    this.action = action;
    return this;
  }

  /**
   * A suggested action following this notification.
   * @return action
   **/
  @ApiModelProperty(value = "A suggested action following this notification.")
  
    public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public Notification timestamp(LocalDateTime timestamp) {
    this.timestamp = timestamp;
    return this;
  }

  /**
   * Timestamp of the notification.
   * @return timestamp
   **/
  @ApiModelProperty(example = "2021-01-15T20:38:45.408Z", required = true, value = "Timestamp of the notification.")
      @NotNull

    @Valid
    public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(LocalDateTime timestamp) {
    this.timestamp = timestamp;
  }

  public Notification severity(String severity) {
    this.severity = severity;
    return this;
  }

  /**
   * Severity of the notification (ERROR, WARNING, INFO, UNSPECIFIED)
   * @return severity
   **/
  @ApiModelProperty(example = "ERROR", value = "Severity of the notification (ERROR, WARNING, INFO, UNSPECIFIED)")
  
    public String getSeverity() {
    return severity;
  }

  public void setSeverity(String severity) {
    this.severity = severity;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Notification notification = (Notification) o;
    return Objects.equals(this.category, notification.category) &&
        Objects.equals(this.code, notification.code) &&
        Objects.equals(this.message, notification.message) &&
        Objects.equals(this.description, notification.description) &&
        Objects.equals(this.fieldName, notification.fieldName) &&
        Objects.equals(this.action, notification.action) &&
        Objects.equals(this.timestamp, notification.timestamp) &&
        Objects.equals(this.severity, notification.severity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(category, code, message, description, fieldName, action, timestamp, severity);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Notification {\n");

    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    fieldName: ").append(toIndentedString(fieldName)).append("\n");
    sb.append("    action: ").append(toIndentedString(action)).append("\n");
    sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
    sb.append("    severity: ").append(toIndentedString(severity)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
