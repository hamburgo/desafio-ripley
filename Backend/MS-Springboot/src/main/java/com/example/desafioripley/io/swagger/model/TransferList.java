package com.example.desafioripley.io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * List movement transfer.
 */
@ApiModel(description = "List movement transfer.")
@Validated


public class TransferList   {
  @JsonProperty("monto")
  private String monto = null;

  @JsonProperty("fecha")
  private String fecha = null;

  @JsonProperty("destino")
  private String destino = null;

  @JsonProperty("tipo")
  private String tipo = null;

  public TransferList monto(String monto) {
    this.monto = monto;
    return this;
  }

  /**
   * monto
   * @return monto
   **/
  @ApiModelProperty(example = "223233", value = "monto")
  
    public String getMonto() {
    return monto;
  }

  public void setMonto(String monto) {
    this.monto = monto;
  }

  public TransferList fecha(String fecha) {
    this.fecha = fecha;
    return this;
  }

  /**
   * Date movement transfer.
   * @return fecha
   **/
  @ApiModelProperty(example = "2021-04-06T02:13:35.099Z", value = "Date movement transfer.")
  
    public String getFecha() {
    return fecha;
  }

  public void setFecha(String fecha) {
    this.fecha = fecha;
  }

  public TransferList destino(String destino) {
    this.destino = destino;
    return this;
  }

  /**
   * Destination customerId transfer.
   * @return destino
   **/
  @ApiModelProperty(example = "16675738-k", value = "Destination customerId transfer.")
  
    public String getDestino() {
    return destino;
  }

  public void setDestino(String destino) {
    this.destino = destino;
  }

  public TransferList tipo(String tipo) {
    this.tipo = tipo;
    return this;
  }

  /**
   * Type movement transfer.
   * @return tipo
   **/
  @ApiModelProperty(example = "Deposito", value = "Type movement transfer.")
  
    public String getTipo() {
    return tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransferList transferList = (TransferList) o;
    return Objects.equals(this.monto, transferList.monto) &&
        Objects.equals(this.fecha, transferList.fecha) &&
        Objects.equals(this.destino, transferList.destino) &&
        Objects.equals(this.tipo, transferList.tipo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(monto, fecha, destino, tipo);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransferList {\n");

    sb.append("    monto: ").append(toIndentedString(monto)).append("\n");
    sb.append("    fecha: ").append(toIndentedString(fecha)).append("\n");
    sb.append("    destino: ").append(toIndentedString(destino)).append("\n");
    sb.append("    tipo: ").append(toIndentedString(tipo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
