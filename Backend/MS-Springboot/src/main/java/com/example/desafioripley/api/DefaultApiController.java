package com.example.desafioripley.api;

import com.example.desafioripley.io.swagger.model.LoginRequest;
import com.example.desafioripley.io.swagger.model.LoginResponse;
import com.example.desafioripley.io.swagger.model.TransfersResponse;
import io.swagger.annotations.ApiParam;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class DefaultApiController implements DefaultApi {

    private final DefaultApiDelegate delegate;

    @org.springframework.beans.factory.annotation.Autowired
    public DefaultApiController(DefaultApiDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public DefaultApiDelegate getDelegate() {
        return delegate;
    }

    public ResponseEntity<TransfersResponse> apiMovementTransfersUserIdGet(@ApiParam(value = "", required=true) @PathVariable("userId") String userId) {
        return delegate.apiMovementTransfersUserIdGet(userId);
    }

    public ResponseEntity<LoginResponse> apiUserLoginPost(@ApiParam(value = "", required=true ) @Valid @RequestBody LoginRequest body) {
        return delegate.apiUserLoginPost(body);
    }

}
