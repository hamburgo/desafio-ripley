package com.example.desafioripley.io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * Login for simulation purposes
 */
@ApiModel(description = "Login for simulation purposes")
@Validated


public class LoginRequest   {
  @JsonProperty("rut")
  private String rut = null;

  @JsonProperty("password")
  private String password = null;

  public LoginRequest rut(String rut) {
    this.rut = rut;
    return this;
  }

  /**
   * Customer rut
   * @return rut
   **/
  @ApiModelProperty(example = "12345678-9", required = true, value = "Customer rut")
      @NotNull

    public String getRut() {
    return rut;
  }

  public void setRut(String rut) {
    this.rut = rut;
  }

  public LoginRequest password(String password) {
    this.password = password;
    return this;
  }

  /**
   * Customer password
   * @return password
   **/
  @ApiModelProperty(example = "12345678-9", required = true, value = "Customer password")
      @NotNull

    public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LoginRequest loginRequest = (LoginRequest) o;
    return Objects.equals(this.rut, loginRequest.rut) &&
        Objects.equals(this.password, loginRequest.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(rut, password);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LoginRequest {\n");

    sb.append("    rut: ").append(toIndentedString(rut)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
