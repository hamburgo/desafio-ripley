package com.example.desafioripley.repository;

import com.example.desafioripley.domain.Transfer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransferRepository extends MongoRepository<Transfer, String> {

    Transfer findTransferByUserId(String id);
}