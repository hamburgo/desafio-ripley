package com.example.desafioripley.controller;

import com.example.desafioripley.api.DefaultApiDelegate;
import com.example.desafioripley.io.swagger.model.LoginRequest;
import com.example.desafioripley.io.swagger.model.LoginResponse;
import com.example.desafioripley.io.swagger.model.TransfersResponse;
import com.example.desafioripley.repository.CustomRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.POST})
public class DesafioRipleyController implements DefaultApiDelegate {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired
    CustomRepository customRepository;

    @Override
    public ResponseEntity<TransfersResponse> apiMovementTransfersUserIdGet(String userId) {
        return ResponseEntity.ok(customRepository.getAllTransfers());
    }

    @Override
    public ResponseEntity<LoginResponse> apiUserLoginPost(LoginRequest body) {
        return null;
    }
}
