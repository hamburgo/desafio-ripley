package com.example.desafioripley.api;

import com.example.desafioripley.io.swagger.model.LoginRequest;
import com.example.desafioripley.io.swagger.model.LoginResponse;
import com.example.desafioripley.io.swagger.model.TransfersResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;


/**
 * A delegate to be called by the {@link DefaultApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
public interface DefaultApiDelegate {

    Logger log = LoggerFactory.getLogger(DefaultApi.class);

    /**
     * @see DefaultApi#apiMovementTransfersUserIdGet
     */
    ResponseEntity<TransfersResponse> apiMovementTransfersUserIdGet(String  userId);

    /**
     * @see DefaultApi#apiUserLoginPost
     */
    ResponseEntity<LoginResponse> apiUserLoginPost(LoginRequest body);

}
