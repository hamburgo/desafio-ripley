package com.example.desafioripley;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioRipleyApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioRipleyApplication.class, args);
	}

}
