package com.example.desafioripley.repository;

import com.example.desafioripley.domain.Transfer;
import com.example.desafioripley.io.swagger.model.TransferList;
import com.example.desafioripley.io.swagger.model.TransfersResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TransferRepositoryImpl implements CustomRepository {

    @Autowired
    TransferRepository transferRepository;

    @Override
    public TransfersResponse getAllTransfers() {
        TransfersResponse transfersResponse = new TransfersResponse();
        List<Transfer> list = transferRepository.findAll();

        list.stream().forEach(data -> {
            TransferList transferList = new TransferList();
            transferList.setMonto(data.getMonto());
            transferList.setFecha(data.getFecha());
            transferList.setDestino(data.getDestino());
            transferList.setTipo(data.getTipo());
            transfersResponse.add(transferList);
        });

        return transfersResponse;
    }
}
