package com.example.desafioripley.io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * Login response
 */
@ApiModel(description = "Login response")
@Validated


public class LoginResponse   {
  @JsonProperty("userId")
  private String userId = null;

  @JsonProperty("status")
  private String status = null;

  @JsonProperty("token")
  private String token = null;

  @JsonProperty("expireIn")
  private String expireIn = null;

  public LoginResponse userId(String userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Customer id
   * @return userId
   **/
  @ApiModelProperty(example = "12323weewedsdd9", value = "Customer id")
  
    public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public LoginResponse status(String status) {
    this.status = status;
    return this;
  }

  /**
   * Status
   * @return status
   **/
  @ApiModelProperty(example = "OK", value = "Status")
  
    public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public LoginResponse token(String token) {
    this.token = token;
    return this;
  }

  /**
   * token
   * @return token
   **/
  @ApiModelProperty(example = "JWT", value = "token")
  
    public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public LoginResponse expireIn(String expireIn) {
    this.expireIn = expireIn;
    return this;
  }

  /**
   * Time expire
   * @return expireIn
   **/
  @ApiModelProperty(example = "time", value = "Time expire")
  
    public String getExpireIn() {
    return expireIn;
  }

  public void setExpireIn(String expireIn) {
    this.expireIn = expireIn;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LoginResponse loginResponse = (LoginResponse) o;
    return Objects.equals(this.userId, loginResponse.userId) &&
        Objects.equals(this.status, loginResponse.status) &&
        Objects.equals(this.token, loginResponse.token) &&
        Objects.equals(this.expireIn, loginResponse.expireIn);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, status, token, expireIn);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LoginResponse {\n");

    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    token: ").append(toIndentedString(token)).append("\n");
    sb.append("    expireIn: ").append(toIndentedString(expireIn)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
