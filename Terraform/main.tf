provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "instancia-1" {
  ami           = "ami-0742b4e673072066f"
  instance_type = "t2.micro"
  user_data     = data.template_file.backend_cloud_init.rendered

  tags = {
    Name = "Instancia 1"
  }

  vpc_security_group_ids = [aws_security_group.sg_1.id]
}

resource "aws_security_group" "sg_1" {
  name = "terraform-sg_1"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 4200
    to_port     = 4200
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "template_file" "backend_cloud_init" {
  template = file("instancia1_script.sh")
  vars = {
    JDK_VERSION = "openjdk-8-jdk"
  }
}
