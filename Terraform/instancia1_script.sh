#!/bin/bash -xe

# Java and Maven Installation on Linux Amazon 2 AMI
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
  yum update -y
  yum install java-1.8.0-openjdk -y
  java -version
  
  sudo wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo
  sudo sed -i s/\$releasever/6/g /etc/yum.repos.d/epel-apache-maven.repo
  sudo yum install -y apache-maven
  mvn --version

  mvn -v
  echo "ok"

  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
  . ~/.nvm/nvm.sh
  
  nvm install node
  npm install -g @angular/cli

  mkdir mygit
 
  #get inside mygit directory
  cd mygit/
  
  #initialize git local repository
  #git clone
  
  #observe .git file is created in your local git repository which
  #contains data and metadata of your local git repository
  ls -a
  ls -a .git/
  
  #check git status
  git status
