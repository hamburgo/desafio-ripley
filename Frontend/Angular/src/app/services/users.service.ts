export class UsersService {
  private users = [
    {
      username: 'Cristian Aguayo',
      rut: '16675738k',
      email: 'icicaf@gmail.com',
    },
    {
      username: 'Alejandro Forteza',
      rut: '16675738-1',
      email: 'admin@scap.cl',
    },
  ];

  agregarUsuario(username: string, rut: string, email: string) {
    this.users.push({ username, rut, email });
  }

  obtenerUsuarios() {
    return [...this.users];
  }
}
